function parseTime(start) {
	const parts = start.match(/(\d+)([A-Za-z]+)/g);

	return parts
		.reduce((accum, timePart) => {
			const [, digit, type] = timePart.match(/([0-9]+)([A-Za-z]+)/);
			let res = 0;
			switch(type) {
				case 's':
					return accum + parseInt(digit, 10);
				case 'm':
					return accum + (parseInt(digit, 10) * 60);
				case 'h':
					return accum + (parseInt(digit, 10) * 60 * 60);
				default:
					console.log(`Unknown time ${timePart} found in 'start' argument`);
			}
		}, 0);
}

function formatTime(secs) {
	let seconds = secs;
	let mins = 0;
	let hours = 0;

	while(seconds > 59) {
		seconds -= 60;
		mins++;
	}

	while(mins > 59) {
		mins -= 60;
		hours++;
	}

	hours = `${hours}`.padStart(2, '0');
	mins = `${mins}`.padStart(2, '0');
	seconds = `${seconds}`.padStart(2, '0');

	return `${hours > 0 ? `${hours}:` : ''}${mins}:${seconds}`;
}

module.exports = {
	parseTime,
	formatTime
};
