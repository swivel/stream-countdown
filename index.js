#!/usr/bin/node

const yargs = require('yargs');
const utils = require('./utils.js');
const fs = require('fs');

const argv = yargs
	.option('start', {
		alias: 's',
		describe: 'The start time for the timer'
	})
	.option('file', {
		alias: 'f',
		describe: 'The file to store the current time'
	})
	.option('delay', {
		alias: 'd',
		describe: 'Delay the start of the timer'
	})
	.demandOption(['start', 'file'], 'You must provide a `start` and `file` for this script to run!')
	.argv;


var timer = utils.parseTime(argv.start);
const delay = utils.parseTime(argv.delay || '0s');

var interval;

fs.writeFileSync(argv.file, utils.formatTime(timer));

setTimeout(() => {
	interval = setInterval(() => {
		timer--;

		fs.writeFileSync(argv.file, utils.formatTime(timer));

		if (timer <= 0) {
			clearInterval(interval);
			console.log('Timer is done!');
		}
	}, 1000);
}, delay * 1000);

